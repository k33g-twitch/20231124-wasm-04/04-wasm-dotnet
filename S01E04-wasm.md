---
marp: true
---
# S01E04

## WebAssembly (Wasm), in the browser with .Net

> - Repo: https://gitlab.com/k33g-twitch/20231124-wasm-04/04-wasm-dotnet
> - 👀 `README.md` => Open it with Docker Development Environment

---
# Agenda

- Intro: .Net & WebAssembly
- Blazor
- WebAssembly Browser App
- Next

---
# Disclaimer

- I stopped developing in .Net in 2006

---
# Microsoft, .Net & WebAssembly

- **Blazor**: https://dotnet.microsoft.com/en-us/apps/aspnet/web-apps/blazor
- **WebAssembly Browser App**
- WebAssembly Console App
- Wasi Console App

---
# Setup
> Windows, Linux, Mac

```bash
wget -O dotnet-sdk.tar.gz ${DOTNET_SDK_URL}
tar -xf dotnet-sdk.tar.gz -C /usr/bin
rm dotnet-sdk.tar.gz

dotnet workload install wasm-tools
dotnet workload install wasm-experimental
```

> - https://dotnet.microsoft.com/en-us/download/dotnet/8.0
> - DOTNET_SDK_URL=https://download.visualstudio.microsoft.com/download/pr/0247681a-1a4a-4a32-a1a6-4149d56af27e/5bcbf1d8189c2649b16d27f5199e04a4/dotnet-sdk-8.0.100-rc.2.23502.2-linux-arm64.tar.gz



---
# Blazor
> https://learn.microsoft.com/en-us/aspnet/core/blazor/hosting-models?view=aspnetcore-8.0

- Blazor Server
- **Blazor WebAssembly** 
- Blazor Hybrid

---
# Blazor WebAssembly

```bash
dotnet new list

dotnet new blazorwasm -o hello-blazor-app

cd hello-blazor-app
dotnet watch
```

---
# WebAssembly Browser App

> ```Csharp
> using System.Runtime.InteropServices.JavaScript;
> // JSObject 😍
> ```

```bash
dotnet new wasmbrowser -o hello-wasm-app
cd hello-wasm-app
dotnet build
dotnet run
# or dotnet watch
```

---
# To read / To try

- Blog: https://laurentkempe.com/
- Blog: https://www.meziantou.net/using-dotnet-code-from-javascript-using-webassembly.htm
- Avalonia: https://docs.avaloniaui.net/docs/next/guides/platforms/how-to-use-web-assembly

---
# Next time(s): Season 02 🎉 

## Wasi

- Plumbing reminders (with Node.js)
- Wasm runtimes and first wasm programs (wasi compliant)
- Host applications: 
  - DIY Part 1: with Wazero
  - DIY Part 2: host functions (with Wazero)
- Application servers
  - Spin Framework
  - Wasm Workers Server


---
# Next time(s): Season 02 🎉 

## Wasi

- Extism 
  - PDK and Extism CLI
  - SDK: make hosts applications (several parts)
- Docker and WebAssembly
  - https://www.youtube.com/watch?v=8qtmJM1r4BI


---

# Next time(s): Season 02 🎉 

## Wasi

- .Net and Wasi (Console application and ASP.Net application)
- Platform
  - Fermyon
  - wasmCloud
- GrainLang
- ...
---
# 👋


