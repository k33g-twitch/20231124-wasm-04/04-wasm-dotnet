using System;
using System.Runtime.InteropServices.JavaScript;

Console.WriteLine("Hello, Browser!");

public partial class MyClass
{
    [JSExport]
    internal static string Greeting(JSObject human)
    {
        
        var text = $"Hello! {human.GetPropertyAsString("Name")}";
        Console.WriteLine(text);
        return text;
    }


}
